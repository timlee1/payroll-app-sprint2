package com.payroll.unitTest.servlet;

import com.payroll.servlet.TimeCardServlet;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.mockito.Mockito.*;

public class TimeCardServletTest {
    @Mock private HttpServletRequest request;
    @Mock private HttpServletResponse response;
    @Mock private RequestDispatcher requestDispatcher;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGet() throws Exception {
        // Arrange
        when(request.getRequestDispatcher(anyString())).thenReturn(requestDispatcher);

        // Act
        new TimeCardServlet().doGet(request, response);

        // Assert
        verify(request).getRequestDispatcher("timeCard.jsp");
        verify(request).setAttribute(eq("timeCards"), anyList());
    }

    @Test
    public void testPostSuccess() throws Exception {
        // Arrange
        when(request.getParameter("employee")).thenReturn("Bob");
        when(request.getParameter("checkInDate")).thenReturn("2018/01/01");
        when(request.getParameter("checkInHour")).thenReturn("10");
        when(request.getParameter("checkOutDate")).thenReturn("2018/01/01");
        when(request.getParameter("checkOutHour")).thenReturn("18");
        when(request.getRequestDispatcher(anyString())).thenReturn(requestDispatcher);

        // Act
        new TimeCardServlet().doPost(request, response);

        // Assert
        verify(request).getRequestDispatcher("timeCard.jsp");
        verify(request).setAttribute(eq("timeCards"), anyList());
        verify(request).setAttribute(eq("isSuccess"), eq(true));
        verify(request, never()).setAttribute(eq("isError"), anyBoolean());
    }

    @Test
    public void testPostError() throws Exception {
        // Arrange
        when(request.getParameter("employee")).thenReturn("Bob");
        when(request.getRequestDispatcher(anyString())).thenReturn(requestDispatcher);

        // Act
        new TimeCardServlet().doPost(request, response);

        // Assert
        verify(request).getRequestDispatcher("timeCard.jsp");
        verify(request).setAttribute(eq("timeCards"), anyList());
        verify(request, never()).setAttribute(eq("isSuccess"), anyBoolean());
        verify(request).setAttribute(eq("isError"), eq(true));
        verify(request).setAttribute(eq("errorCause"), anyString());
    }
}
