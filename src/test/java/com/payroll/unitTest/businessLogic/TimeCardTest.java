package com.payroll.unitTest.businessLogic;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.payroll.businessLogic.*;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TimeCardTest {
    private BusinessCalendar bCalendar;

    @Before
    public void setup() {
        final List<Date> holidays = new ArrayList<Date>();
        holidays.add(TimeUtils.buildDate(2014, 1, 1));
        holidays.add(TimeUtils.buildDate(2014, 1, 2));
        // build business calendar with holidays list above

        this.bCalendar = new BusinessCalendar() {
            public boolean isNationalHoliday(Date date) {
                for (Date tdate: holidays){
                    if (TimeUtils.isSameDay(tdate, date)) return true;
                }
                return false;
            }
        };
    }

    @Test
    public void testMorningOnly() {
        // Arrange
        TimeCard timecard = new TimeCard("2014/04/11 09:00:00", "2014/04/11 12:00:00");

        // Act
        timecard.setBusinessCalendar(this.bCalendar);
        timecard.process();

        // Assert
        int expectedBaseHour = 3;
        int expectedOverTime = 0;
        assertEquals(timecard.getBaseHours(), expectedBaseHour);
        assertEquals(timecard.getOverTimeHours(), expectedOverTime);
    }

    @Test
    public void testFullDayNoOTOnly() {
        // Arrange
        TimeCard timecard = new TimeCard("2014/04/11 09:00:00", "2014/04/11 18:00:00");

        // Act
        timecard.setBusinessCalendar(this.bCalendar);
        timecard.process();

        // Assert
        int expectedBaseHour = 8;
        int expectedOverTime = 0;
        assertEquals(expectedBaseHour, timecard.getBaseHours());
        assertEquals(expectedOverTime, timecard.getOverTimeHours());
    }

    @Test
    public void testFullDayWithOT() {
        // Arrange
        TimeCard timecard = new TimeCard("2014/04/11 10:00:00", "2014/04/11 22:00:00");

        // Act
        timecard.setBusinessCalendar(this.bCalendar);
        timecard.process();

        // Assert
        int expectedBaseHour = 8;
        int expectedOverTime = 3;
        assertEquals(expectedBaseHour, timecard.getBaseHours());
        assertEquals(expectedOverTime, timecard.getOverTimeHours());
    }

    @Test
    public void testWorkingOnHoliday() {
        // Arrange
        TimeCard timecard = new TimeCard("2014/01/01 9:00:00", "2014/01/01 17:00:00");

        // Act
        timecard.setBusinessCalendar(this.bCalendar);
        timecard.process();

        // Assert
        int expectedBaseHour = 0;
        int expectedOverTime = 7;
        assertEquals(expectedBaseHour, timecard.getBaseHours());
        assertEquals(expectedOverTime, timecard.getOverTimeHours());
    }
}
