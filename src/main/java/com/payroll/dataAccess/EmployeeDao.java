package com.payroll.dataAccess;

import com.payroll.businessLogic.HourlyEmployee;
import com.payroll.businessLogic.TimeCard;
import com.payroll.businessLogic.TimeUtils;

import java.util.ArrayList;

/**
 * Employee DAO
 */
public class EmployeeDao {
    private static ArrayList<HourlyEmployee> hourlyEmployees;

    private static EmployeeDao instance  = new EmployeeDao();

    private EmployeeDao() {
        try {
            hourlyEmployees = new ArrayList<HourlyEmployee>();

            HourlyEmployee bob = new HourlyEmployee("Bob", 120);
            bob.addTimeCard(new TimeCard(TimeUtils.buildDate("2018/11/27 09:00:00"), TimeUtils.buildDate("2018/11/27 18:00:00")));
            bob.addTimeCard(new TimeCard(TimeUtils.buildDate("2018/11/28 10:00:00"), TimeUtils.buildDate("2018/11/28 19:00:00")));
            hourlyEmployees.add(bob);

            HourlyEmployee mary = new HourlyEmployee("Mary", 130);
            mary.addTimeCard(new TimeCard(TimeUtils.buildDate("2018/11/29 08:00:00"), TimeUtils.buildDate("2018/11/29 20:00:00")));
            mary.addTimeCard(new TimeCard(TimeUtils.buildDate("2018/11/30 10:00:00"), TimeUtils.buildDate("2018/11/30 23:00:00")));
            hourlyEmployees.add(mary);

            HourlyEmployee john = new HourlyEmployee("John", 140);
            john.addTimeCard(new TimeCard(TimeUtils.buildDate("2018/12/01 10:00:00"), TimeUtils.buildDate("2018/12/01 12:00:00")));
            john.addTimeCard(new TimeCard(TimeUtils.buildDate("2018/12/02 10:00:00"), TimeUtils.buildDate("2018/12/02 15:00:00")));
            hourlyEmployees.add(john);


        } catch (Exception ex) {
        }
    }

    public static EmployeeDao getInstance() {
        return instance;
    }

    public ArrayList<HourlyEmployee> getHourlyEmployees() {
        return hourlyEmployees;
    }
}